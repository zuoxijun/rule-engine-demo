package com.example.ruleengine.mydrools;

import lombok.*;

@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MemberPositionLimit {
    /**
     * 会员号
     */
    private String memberId;
    /**
     * 会员类型
     */
    private String memberType;
    /**
     * 产品
     */
    private String productId;
    /**
     * 多头限仓（手）
     */
    private Integer longLimit;
    /**
     * 空头限仓(手)
     */
    private Integer shortLimit;
}
